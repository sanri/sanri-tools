package com.sanri.tools.modules.security.service;

import com.sanri.tools.modules.core.security.UserOpenService;
import com.sanri.tools.modules.core.security.UserService;
import com.sanri.tools.modules.core.security.entitys.UserProfile;
import com.sanri.tools.modules.security.service.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class UserOpenServiceImpl implements UserOpenService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserProfile profile(String username) throws IOException {
        UserProfile profile = userRepository.profile(username);
        return profile;
    }
}

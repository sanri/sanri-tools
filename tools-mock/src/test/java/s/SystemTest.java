package s;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.springframework.util.PropertyPlaceholderHelper;

import java.text.ParseException;
import java.util.Date;
import java.util.Properties;

public class SystemTest {

    @Test
    public void genScript() throws ParseException {
        String template = "delete /*+QUERY_TIMEOUT(240000000)*/ from scen_cap_img_c a where  a.VALI_FLAG=0 and a.CAP_TIME <= STR_TO_DATE(\n" +
                "    '${endtime}',\n" +
                "    '%Y-%m-%d %H:%i:%s'\n" +
                ") and a.CAP_TIME >= STR_TO_DATE(\n" +
                "    '${starttime}',\n" +
                "    '%Y-%m-%d %H:%i:%s'\n" +
                ");";

        String template2 = "delete /*+QUERY_TIMEOUT(240000000)*/ from scen_cap_img_c a where  a.VALI_FLAG=0 and a.CAP_TIME < STR_TO_DATE(\n" +
                "    '${endtime}',\n" +
                "    '%Y-%m-%d %H:%i:%s'\n" +
                ") and a.CAP_TIME >= STR_TO_DATE(\n" +
                "    '${starttime}',\n" +
                "    '%Y-%m-%d %H:%i:%s'\n" +
                ");";

        PropertyPlaceholderHelper placeholderHelper = new PropertyPlaceholderHelper("${","}");

        Date datestart = DateUtils.parseDate("2023-07-22", "yyyy-MM-dd");
        Date dateend = DateUtils.addMinutes(datestart, 10);
//        System.out.println(date);
        
        String format = "yyyy-MM-dd HH:mm:ss";
        
        Properties properties = new Properties();
        
        int interval = 10;

        for (int i = 0; i < 144; i++) {
            Date start = DateUtils.addMinutes(datestart, i * interval);
            Date end = DateUtils.addMinutes(dateend, i * interval);

            properties.setProperty("starttime", DateFormatUtils.format(start, format));
            properties.setProperty("endtime", DateFormatUtils.format(end, format));

            if (i == 143){
                String s = placeholderHelper.replacePlaceholders(template2, properties);
                System.out.println(s);
            }else {
                String s = placeholderHelper.replacePlaceholders(template, properties);
                System.out.println(s);
            }


            System.out.println();
        }
        

    }
}

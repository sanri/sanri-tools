# 协议模拟器

## http 请求模拟 
* 支持设置 http 接口和响应数据
* 支持从响应体中以一定策略来选择响应体, 支持的策略有 first, last, random, 轮询
* 响应体的配置需要在 系统管理 -> 通用配置项 -> response.yaml 模块进行配置

## websocket 数据模拟

## tcp & udp 数据模拟
可以模拟服务端和客户端, 支持 hex 和 ascii 序列化数据
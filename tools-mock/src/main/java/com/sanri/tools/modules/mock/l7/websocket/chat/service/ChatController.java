package com.sanri.tools.modules.mock.l7.websocket.chat.service;

import com.sanri.tools.modules.core.dtos.param.PageParam;
import com.sanri.tools.modules.core.security.entitys.UserProfile;
import com.sanri.tools.modules.mock.l7.websocket.chat.domain.ChatContact;
import com.sanri.tools.modules.mock.l7.websocket.chat.domain.Message;
import com.sanri.tools.modules.mock.l7.websocket.chat.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/chat")
@Validated
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private ChatGroupService chatGroupService;

    @Autowired
    private MediaRepository mediaRepository;

    /**
     * 联系人列表
     * @return
     */
    @GetMapping("/contacts")
    public List<ChatContact> contacts() throws IOException {
        List<ChatContact> chatContacts = chatService.contacts();
        return chatContacts;
    }

    /**
     * 添加联系人
     * @param contact
     */
    @PostMapping("/contact/add")
    public void addContact(@NotBlank String contact) throws IOException {
        chatService.addContact(contact);
    }

    /**
     * 联系消息
     * @param contact
     * @param pageParam
     * @return
     * @throws IOException
     */
    @GetMapping("/messages")
    public List<Message> messages(@NotBlank String contact, boolean group, PageParam pageParam) throws IOException {
        List<Message> messages = chatService.messages(contact, pageParam, group);
        return messages;
    }

    /**
     * 查看某个日期的历史消息
     * @param contact
     * @param group
     * @param date
     * @return
     */
    @GetMapping("/history/messages")
    public List<Message> historyMessages(@NotBlank String contact, boolean group , PageParam pageParam, String date){
        return chatService.historyMessages(contact, pageParam, group, date);
    }

    /**
     * 获取聊天日期列表
     * @param contact
     * @param group
     * @return
     */
    @GetMapping("/message/dates")
    public Set<String> messageInDates(@NotBlank String contact, boolean group){
        return chatService.messageListDates(contact, group);
    }

    /**
     * 开启 chat
     * @throws IOException
     */
    @PostMapping("/openChat")
    public void openChat() throws IOException {
        chatService.openChat();
    }

    /**
     * 客户端图片/文件上传
     * @return
     */
    @PostMapping({"/upload/file","/upload/image","/upload/sound","/upload/video"})
    public String uploadMedia(@RequestParam("file") MultipartFile file) throws IOException {
        return mediaRepository.storageMedia(file);
    }

    /**
     * http 方式发送消息
     * @param message
     */
    @PostMapping("/sendMessage")
    public void sendMessage(@RequestBody Message message) throws IOException {
        chatService.sendMessage(message);
    }

    /**
     * 用户拥有的组织
     * @return
     */
    @GetMapping("/user/groups")
    public List<String> userGroups(){
        return chatService.userGroups();
    }

    @PostMapping("/user/joinGroup")
    public void joinGroup(@NotBlank String groupName) throws IOException {
        chatService.joinGroup(groupName);
    }

    @PostMapping("/user/leaveGroup")
    public void leaveGroup(@NotBlank String groupName) throws IOException {
        chatService.leaveGroup(groupName);
    }

    /**
     * 创建组
     * @param groupName
     */
    @PostMapping("/group/create")
    public void createGroup(@RequestBody UserProfile userProfile) throws IOException {
        chatGroupService.createGroup(userProfile);
    }

    /**
     * 组属性
     * @param groupName
     * @return
     */
    @GetMapping("/group/profile")
    public UserProfile groupProfile(@NotBlank String groupName) throws IOException {
        UserProfile profile = chatGroupService.profile(groupName);
        return profile;
    }

    /**
     * 解散组
     * @param groupName
     */
    @PostMapping("/group/drop")
    public void dropGroup(@NotBlank String groupName) throws IOException {
        chatGroupService.dropGroup(groupName);
    }

    /**
     * 组添加用户
     * @param groupName
     * @param username
     */
    @PostMapping("/group/addUser")
    public void groupAddUser(@NotBlank String groupName, @NotBlank String username) throws IOException {
        chatGroupService.addUser(groupName, username);
    }

    /**
     * 组删除用户
     * @param groupName
     * @param username
     */
    @PostMapping("/group/removeUser")
    public void groupRemoveUser(@NotBlank String groupName, @NotBlank String username) throws IOException {
        chatGroupService.removeUser(groupName, username);
    }

    /**
     * 组用户列表
     * @param groupName
     * @return
     */
    @GetMapping("/group/users")
    public List<GroupUser> groupUsers(@NotBlank String groupName) throws IOException {
        List<GroupUser> users = chatGroupService.users(groupName);
        return users;
    }

    @GetMapping("/groupList")
    public List<UserProfile> groupList() throws IOException {
        return chatGroupService.groupList();
    }

}

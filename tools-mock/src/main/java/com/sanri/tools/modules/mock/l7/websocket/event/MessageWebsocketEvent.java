package com.sanri.tools.modules.mock.l7.websocket.event;

/**
 * @description:
 * @author: huangzhengri
 * @time: 2023-05-26 16:53
 */
public class MessageWebsocketEvent extends WebsocketEvent {
    private String message;
    /**
     * source 指代  CustomWebsocketClient 对象
     *
     * @param source
     */
    public MessageWebsocketEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    @Override
    public MessageType messageType() {
        return MessageType.MESSAGE;
    }

    public String getMessage() {
        return message;
    }
}

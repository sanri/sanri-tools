package com.sanri.tools.modules.mock.l7.websocket.event;

import lombok.extern.slf4j.Slf4j;
import org.java_websocket.handshake.Handshakedata;
import org.java_websocket.handshake.ServerHandshake;

@Slf4j
public class ConnectWebsocketEvent extends WebsocketEvent{
    private Handshakedata handshakedata;

    /**
     * source 指代  CustomWebsocketClient 对象
     *
     * @param source
     */
    public ConnectWebsocketEvent(Object source, Handshakedata handshakedata) {
        super(source);
        this.handshakedata = handshakedata;
    }

    @Override
    public MessageType messageType() {
        return MessageType.CONNECT;
    }

    public Handshakedata getHandshakedata() {
        return handshakedata;
    }
}

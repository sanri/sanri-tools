package com.sanri.tools.modules.mock.l7.websocket.chat.service.ws;

import com.sanri.tools.modules.core.security.dtos.ThinUser;
import lombok.Data;
import org.java_websocket.WebSocket;

@Data
public class ChatSession {
    private WebSocket webSocket;
    private ThinUser thinUser;
    private String token;

    public ChatSession() {
    }

    public ChatSession(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public ChatSession(WebSocket webSocket, ThinUser thinUser) {
        this.webSocket = webSocket;
        this.thinUser = thinUser;
    }
}

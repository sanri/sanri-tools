package com.sanri.tools.modules.mock.l7.websocket.chat.domain;

import com.sanri.tools.modules.core.security.entitys.UserProfile;
import lombok.Data;

@Data
public class ChatContact {
    private String id;
    private String displayName;
    private String avatar;
    private String index;
    private int unread;
    private long lastSendTime;
    private String lastContent;
    /**
     * 是否群组
     */
    private boolean group;

    public ChatContact() {
    }

    public ChatContact(UserProfile userProfile) {
        this.id = userProfile.getUsername();
        this.displayName = userProfile.getDisplayName();
        this.avatar = userProfile.getAvatar();
    }
}

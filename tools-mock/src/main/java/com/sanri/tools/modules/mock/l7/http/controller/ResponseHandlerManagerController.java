package com.sanri.tools.modules.mock.l7.http.controller;

import com.sanri.tools.modules.mock.l7.http.service.ResponseHandlerManagerService;
import com.sanri.tools.modules.mock.l7.http.service.dtos.UrlBindResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.Collection;

/**
 * 响应体管理
 */
@RestController
@Slf4j
@RequestMapping("/mock")
public class ResponseHandlerManagerController {

    @Autowired
    private ResponseHandlerManagerService responseHandlerManagerService;

    /**
     * 模拟的地址列表
     * @return
     */
    @GetMapping("/list/urlConfigs")
    public Collection<UrlBindResponseDto> list(){
        return responseHandlerManagerService.list();
    }

    /**
     * 模拟配置修改/新加
     * @param urlBindResponseDto
     */
    @PostMapping("/edit/urlConfig")
    public void editUrlConfig(@RequestBody UrlBindResponseDto urlBindResponseDto){
        responseHandlerManagerService.editUrlResponseBind(urlBindResponseDto);
    }

    /**
     * 删除一个 url 配置
     * @param url
     */
    @PostMapping("/delete/urlConfig")
    public void deleteUrlConfig(@NotBlank String url){
        responseHandlerManagerService.deleteUrlConfig(url);
    }
}

package com.sanri.tools.modules.mock.l7.websocket;

import com.sanri.tools.modules.mock.l7.websocket.client.WebsocketClientService;
import com.sanri.tools.modules.mock.l7.websocket.server.WebsocketServerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.net.URISyntaxException;

@RestController
@Slf4j
@RequestMapping("/websocket/mock")
@Validated
public class WebsocketController {

    @Autowired
    private WebsocketClientService websocketClientService;

    @Autowired
    private WebsocketServerService websocketServerService;

    @PostMapping("/client/connect")
    public void connectServer(@NotBlank String wsurl) throws URISyntaxException {
        websocketClientService.connect(wsurl);
    }

    @PostMapping("/client/sendMessage")
    public void sendMessageToServer(@NotBlank String wsurl, @RequestBody String message) throws URISyntaxException {
        websocketClientService.sendMessageToServer(wsurl, message);
    }

    public void closeClient(){

    }

    @PostMapping("/server/start")
    public void startServer(String port){
        final int portInt = NumberUtils.toInt(port, -1);
        websocketServerService.startServer(portInt,null);
    }



}

package com.sanri.tools.modules.mock.l7.http.configs;

import com.sanri.tools.modules.mock.l7.http.filter.MockFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean mockFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new MockFilter());
        // order -> 1
        registrationBean.setOrder(1);
        // filter all request start with [/vmock]
        registrationBean.addUrlPatterns("/vmock/*");
        return registrationBean;
    }

}

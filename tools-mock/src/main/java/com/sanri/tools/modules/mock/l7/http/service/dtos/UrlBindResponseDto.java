package com.sanri.tools.modules.mock.l7.http.service.dtos;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UrlBindResponseDto {
    protected String url;
    protected String group;
    protected String describe;
    protected List<String> responseConfigNames = new ArrayList<>();
    protected String strategy;

    public UrlBindResponseDto() {
    }

    public UrlBindResponseDto(String url, String group, String describe, List<String> responseConfigNames, String strategy) {
        this.url = url;
        this.group = group;
        this.describe = describe;
        this.responseConfigNames = responseConfigNames;
        this.strategy = strategy;
    }
}

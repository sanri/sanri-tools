package com.sanri.tools.modules.mock.l7.http.service.interceptors;

import java.util.HashMap;
import java.util.Map;

/**
 * 中间数据, 用于拦截器中间值数据
 */
public class IntermediateData {
    private Map<String,Object> data = new HashMap<>();

    public Object getAttribute(String key){
        return data.get(key);
    }

    public <T> T getAttribute(String key, Class<T> clazz){
        final Object object = data.get(key);
        if (object == null){
            return null;
        }
        return clazz.cast(object);
    }

    public void setAttribute(String key, Object object){
        data.put(key, object);
    }
}

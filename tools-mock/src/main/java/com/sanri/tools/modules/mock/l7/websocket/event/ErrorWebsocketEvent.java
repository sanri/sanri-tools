package com.sanri.tools.modules.mock.l7.websocket.event;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorWebsocketEvent extends WebsocketEvent{
    private Exception exception;
    /**
     * source 指代  CustomWebsocketClient 对象
     *
     * @param source
     */
    public ErrorWebsocketEvent(Object source, Exception exception) {
        super(source);
        this.exception = exception;
    }

    @Override
    public MessageType messageType() {
        return MessageType.ERROR;
    }

    public Exception getException() {
        return exception;
    }
}

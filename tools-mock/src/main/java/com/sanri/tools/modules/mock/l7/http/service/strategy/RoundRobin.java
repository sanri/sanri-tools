package com.sanri.tools.modules.mock.l7.http.service.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RoundRobin {
    private List<String> sources = new ArrayList<>();
    private static final AtomicInteger indexAtomic = new AtomicInteger(0);

    public RoundRobin(List<String> sources) {
        this.sources = sources;
    }

    /**
     * 选择一个源
     * @return
     */
    public String choseOne(){
        if (indexAtomic.get() >= sources.size()) {
            indexAtomic.set(0);
        }
        String server = sources.get(indexAtomic.getAndIncrement());
        return server;
    }
}

package com.sanri.tools.modules.mock.l7.websocket.chat.service.ws.message;

import org.java_websocket.WebSocket;

import java.io.IOException;

public enum  SocketMessageCommand {
    BIND("bind"), CHAT("chat");
    private String command;

    SocketMessageCommand(String command) {
        this.command = command;
    }

    public static SocketMessageCommand parse(String command) {
        for (SocketMessageCommand value : SocketMessageCommand.values()) {
            if (value.command.equals(command)){
                return value;
            }
        }
        return null;
    }

    public String getCommand() {
        return command;
    }

    public interface MessageHandler{
        void handle(WebSocket conn, String message) throws IOException;
    }
}

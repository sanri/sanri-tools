package com.sanri.tools.modules.mock.l7.websocket.chat.service;

import com.sanri.tools.modules.core.security.entitys.UserProfile;
import lombok.Data;

public class GroupUser {
    private UserProfile userProfile;
    /**
     * 级别
     * 1: 超级管理员
     * 2: 管理员
     * 0: 平民
     */
    private int level;

    public GroupUser() {
    }

    public GroupUser(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getId() {
        return userProfile.getId();
    }

    public String getDisplayName() {
        return userProfile.getDisplayName();
    }

    public String getAvatar() {
        return userProfile.getAvatar();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}

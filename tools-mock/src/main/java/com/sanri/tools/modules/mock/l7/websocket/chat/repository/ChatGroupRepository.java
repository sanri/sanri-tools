package com.sanri.tools.modules.mock.l7.websocket.chat.repository;

import com.alibaba.fastjson.JSON;
import com.sanri.tools.modules.core.exception.ToolException;
import com.sanri.tools.modules.core.security.entitys.UserProfile;
import com.sanri.tools.modules.core.service.file.FileManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * $datadir
 *   chat
 *     groups
 *       $group 组配置
 *         info 组信息
 *         admins 管理员列表
 *         users[Dir]
 *           $username
 *
 */
@Repository
@Slf4j
public class ChatGroupRepository {

    @Autowired
    private FileManager fileManager;

    /**
     * 创建群聊
     * @param userProfile 组信息
     * @param admin 管理员
     */
    public void createGroup(UserProfile userProfile, String admin) throws IOException {
        File groupDir = groupDir(userProfile.getUsername());
        groupDir.mkdirs();

        File info = new File(groupDir, "info");
        info.createNewFile();
        FileUtils.writeStringToFile(info, JSON.toJSONString(userProfile), StandardCharsets.UTF_8);

        File admins = new File(groupDir, "admins");
        FileUtils.writeLines(admins, Arrays.asList(admin));
    }

    /**
     * 管理员列表, 第一个人是超级管理员
     * @param groupName
     * @param admins
     * @throws IOException
     */
    public void setAdmins(String groupName, List<String> admins) throws IOException {
        File groupDir = groupDir(groupName);
        File adminsFile = new File(groupDir, "admins");
        FileUtils.writeLines(adminsFile, admins, StandardCharsets.UTF_8.name());
    }

    public List<String> getAdmins(String groupName) throws IOException {
        File groupDir = groupDir(groupName);
        File adminsFile = new File(groupDir, "admins");
        return FileUtils.readLines(adminsFile, StandardCharsets.UTF_8);
    }

    public boolean existGroup(String groupName){
        File groupDir = groupDir(groupName);
        return groupDir.exists();
    }

    public void removeGroup(String groupName){
        File groupDir = groupDir(groupName);
        groupDir.delete();
    }

    public void addUser(String groupName, String username) throws IOException {
        if (!existGroup(groupName)){
            throw new ToolException("不存在组: "+ groupName);
        }
        File userDir = groupUserDir(groupName);
        userDir.mkdirs();
        new File(userDir, username).createNewFile();
    }

    public void removeUser(String groupName, String username) throws IOException {
        if (!existGroup(groupName)){
            throw new ToolException("不存在组: "+ groupName);
        }
        File userDir = groupUserDir(groupName);
        userDir.mkdirs();
        new File(userDir, username).delete();
    }

    public boolean existUser(String groupName, String username) throws IOException {
        if (!existGroup(groupName)){
            throw new ToolException("不存在组: "+ groupName);
        }
        File userDir = groupUserDir(groupName);
        userDir.mkdirs();
        return new File(userDir, username).exists();
    }


    public File groupUserDir(String groupName){
        File groupDir = groupDir(groupName);
        return new File(groupDir, "users");
    }

    public File groupDir(String groupName){
        File dir = fileManager.mkDataDir("chat/groups/");
        File file = new File(dir, groupName);
        return file;
    }

    public List<String> groupUsers(String groupName) {
        File userDir = groupUserDir(groupName);
        if (userDir.exists() && userDir.list() != null) {
            return Arrays.asList(userDir.list());
        }
        return new ArrayList<>();
    }

    public UserProfile profile(String groupName) throws IOException {
        File groupDir = groupDir(groupName);
        File info = new File(groupDir, "info");
        String s = FileUtils.readFileToString(info, StandardCharsets.UTF_8);
        return JSON.parseObject(s, UserProfile.class);
    }

    public List<UserProfile> groups() throws IOException {
        List<UserProfile> userProfiles = new ArrayList<>();

        File dir = fileManager.mkDataDir("chat/groups/");
        String[] list = dir.list();
        for (String group : list) {
            UserProfile profile = profile(group);
            userProfiles.add(profile);
        }
        return userProfiles;
    }
}

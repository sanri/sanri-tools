package com.sanri.tools.modules.mock.l7.websocket.event;

import org.springframework.context.ApplicationEvent;

/**
 * @description:
 * @author: huangzhengri
 * @time: 2023-05-26 16:48
 */
public abstract class WebsocketEvent extends ApplicationEvent {

    /**
     * source 指代  CustomWebsocketClient 对象
     * @param source
     */
    public WebsocketEvent(Object source) {
        super(source);

    }

    public enum MessageType{
        MESSAGE, CONNECT, CLOSE, ERROR
    }

    public abstract MessageType messageType();
}

package com.sanri.tools.modules.mock.l7.websocket.chat.service;

import com.sanri.tools.modules.core.security.entitys.UserProfile;
import com.sanri.tools.modules.mock.l7.websocket.chat.domain.Message;

public class OutMessage {
    private Message message;

    private UserProfile fromUser;
    private String toContactId;

    public OutMessage() {
    }
    public OutMessage(Message message) {
        this.message = message;
        this.toContactId = message.getFromContactId();
    }

    public String getId(){
        return message.getId();
    }

    public String getStatus(){
        return message.getStatus();
    }

    public String getType() {
        return message.getType();
    }

    public long getSendTime() {
        return message.getSendTime();
    }

    public String getContent() {
        return message.getContent();
    }

    public String getToContactId() {
        return toContactId;
    }

    public void setToContactId(String toContactId) {
        this.toContactId = toContactId;
    }

    public String getFromContactId() {
        return message.getFromContactId();
    }

    public Long getFileSize() {
        return message.getFileSize();
    }

    public String getFileName() {
        return message.getFileName();
    }

    public boolean isGroup() {
        return message.isGroup();
    }

    public UserProfile getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserProfile fromUser) {
        this.fromUser = fromUser;
    }
}

package com.sanri.tools.modules.mock.l7.http.service.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 请求日志记录数据
 */
@Data
public class RequestLogDto {
    /**
     * url 配置
     */
    private UrlBindResponseDto urlBindResponseDto;
    /**
     * 请求详情
     */
    private RequestDetail requestDetail;
    /**
     * 响应详细
     */
    private ResponseConfig.ResponseDetail responseDetail;
    /**
     * ip
     */
    private String ip;
    /**
     * 调用开始时间与结束时间
     */
    private TimeInfo timeInfo = new TimeInfo();
    /**
     * 最终命中的 url , 有可能是配置的模板 url
     */
    private String hitUrl;
    /**
     * 出错信息, 没有信息代表没出错
     */
    private String errorMsg;

    /**
     * 请求详情
     */
    @Data
    public static final class RequestDetail{
        private String query;
        private List<ResponseConfig.NameValuePair> headers = new ArrayList<>();
        private String body;

        public RequestDetail() {
        }

        public RequestDetail(String query, List<ResponseConfig.NameValuePair> headers, String body) {
            this.query = query;
            this.headers = headers;
            this.body = body;
        }
    }

    @Data
    public static final class TimeInfo{
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss.SSS")
        private Date begin;
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss.SSS")
        private Date end;
    }

    public RequestLogDto() {
    }

    public RequestLogDto(UrlBindResponseDto urlBindResponseDto, ResponseConfig.ResponseDetail responseDetail) {
        this.urlBindResponseDto = urlBindResponseDto;
        this.responseDetail = responseDetail;
    }
}

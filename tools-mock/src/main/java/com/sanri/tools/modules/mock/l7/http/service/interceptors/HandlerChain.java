package com.sanri.tools.modules.mock.l7.http.service.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.omg.PortableInterceptor.Interceptor;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 响应处理链条
 */
@Component
@Slf4j
public class HandlerChain extends ResponseInterceptor.Chain {

    public HandlerChain(List<ResponseInterceptor> interceptors) {
        this.interceptors = interceptors;
    }

    public HandlerChain() {
    }
}

package com.sanri.tools.modules.mock.l7.http.service.dtos;

import lombok.Getter;

@Getter
public class UrlBindAndResponseConfig {
    private UrlBindResponseDto urlBindResponseDto;
    private ResponseConfig responseConfig;

    public UrlBindAndResponseConfig(UrlBindResponseDto urlBindResponseDto, ResponseConfig responseConfig) {
        this.urlBindResponseDto = urlBindResponseDto;
        this.responseConfig = responseConfig;
    }
}

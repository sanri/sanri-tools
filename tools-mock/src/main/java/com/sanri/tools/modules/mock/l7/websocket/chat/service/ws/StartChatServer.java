package com.sanri.tools.modules.mock.l7.websocket.chat.service.ws;

import com.sanri.tools.modules.core.security.UserService;
import com.sanri.tools.modules.mock.l7.websocket.chat.service.ws.message.SocketMessageCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class StartChatServer implements InitializingBean {
    @Autowired(required = false)
    private Map<String, SocketMessageCommand.MessageHandler> messageHandlerMap = new HashMap<>();

    @Override
    public void afterPropertiesSet() throws Exception {
        ChatWebsocketServer chatWebsocketServer = new ChatWebsocketServer();
        chatWebsocketServer.setMessageHandlerMap(messageHandlerMap);
        chatWebsocketServer.start();
    }
}

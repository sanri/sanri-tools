package com.sanri.tools.modules.mock.l7.websocket.chat.service.ws.message;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.WebSocket;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("chat/ping")
@Slf4j
public class PingMessageHandler implements SocketMessageCommand.MessageHandler {
    @Override
    public void handle(WebSocket conn, String message) throws IOException {
        conn.send(JSON.toJSONString(new PingMessage()));
    }

    @Data
    public static final class PingMessage{
        private String command;

        public PingMessage() {
            this.command = "chat/ping";
        }

        public PingMessage(String command) {
            this.command = command;
        }
    }
}

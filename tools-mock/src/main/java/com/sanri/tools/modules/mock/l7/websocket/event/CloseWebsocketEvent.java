package com.sanri.tools.modules.mock.l7.websocket.event;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CloseWebsocketEvent extends WebsocketEvent{
    private int code;
    private String reason;
    private boolean remote;

    /**
     * source 指代  CustomWebsocketClient 对象
     *
     * @param source
     */
    public CloseWebsocketEvent(Object source, int code, String reason, boolean remote) {
        super(source);
        this.code = code;
        this.reason = reason;
        this.remote = remote;
    }

    @Override
    public MessageType messageType() {
        return MessageType.CLOSE;
    }

    public int getCode() {
        return code;
    }

    public String getReason() {
        return reason;
    }

    public boolean isRemote() {
        return remote;
    }
}

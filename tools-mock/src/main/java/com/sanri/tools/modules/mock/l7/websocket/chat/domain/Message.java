package com.sanri.tools.modules.mock.l7.websocket.chat.domain;

import com.sanri.tools.modules.core.utils.SnowflakeIdWorker;
import lombok.Data;

import java.util.Date;

@Data
public class Message {
    private String id;
    private String status;
    private String type;
    private long sendTime;
    private String content;
    private String toContactId;
    private String fromContactId;
    private Long fileSize;
    private String fileName;
    private boolean group;

    public Message() {
        this.sendTime = new Date().getTime();
    }

    public Message(String type, String content, String toContactId, String fromContactId) {
        this.id = SnowflakeIdWorker.getSnowflakeId();
        this.status = "succeed";
        this.type = type;
        this.content = content;
        this.toContactId = toContactId;
        this.fromContactId = fromContactId;
        this.sendTime = new Date().getTime();
    }
}

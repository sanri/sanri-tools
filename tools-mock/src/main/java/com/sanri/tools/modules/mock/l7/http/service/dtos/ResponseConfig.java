package com.sanri.tools.modules.mock.l7.http.service.dtos;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class ResponseConfig {
    private String name;
    private boolean enable;
    /**
     * 排序号, 当多个响应使用一个配置时的优先级
     */
    private int order;

    private ResponseDetail responseDetail;

    @Data
    public static final class ResponseDetail {
        private int code;
        private String method;
        private List<Map> headers = new ArrayList<>();
        private String body;

        /**
         * 语言标识, 如果有就会设置到前端
         */
        private String locale;
    }
    @Data
    public static final class NameValuePair{
        private String name;
        private String value;

        public NameValuePair() {
        }

        public NameValuePair(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

}

package com.sanri.tools.modules.mock.l7.websocket.chat.configs;

import com.sanri.tools.modules.core.service.file.FileManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;

@Component
@Slf4j
public class MvcMediaConfiguration implements WebMvcConfigurer {
    @Autowired
    private FileManager fileManager;

    /**
     * 添加聊天文件,图片映射
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        File dir = fileManager.mkDataDir("chat/media");
        log.info("映射本地媒体路径为: {}",dir.getAbsolutePath());

        // addResourceLocations 必须要用 URL 形式
        registry.addResourceHandler("/chat/media/**")
                .addResourceLocations(dir.toURI().toString());
    }
}
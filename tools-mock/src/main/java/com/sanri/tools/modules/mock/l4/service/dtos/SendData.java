package com.sanri.tools.modules.mock.l4.service.dtos;

import lombok.Data;

@Data
public class SendData {
    private String ascii;
    private String hex;
}

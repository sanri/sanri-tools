package com.sanri.tools.modules.mock.l7.http.service.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Date;

@Data
public class RequestLog {
    private String id;
    private String hitUrl;
    private String urlPattern;
    private String ip;
    private String method;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date time;

    public RequestLog() {
    }

    public RequestLog(RequestLogDto requestLogDto) {
        this.hitUrl = requestLogDto.getHitUrl();
        this.ip = requestLogDto.getIp();
        this.method = requestLogDto.getResponseDetail().getMethod();
        this.time = requestLogDto.getTimeInfo().getBegin();
        this.urlPattern = requestLogDto.getUrlBindResponseDto().getUrl();
    }

    public static RequestLog deSerializer(String line) {
        final String[] items = StringUtils.splitPreserveAllTokens(line,"|", 6);
        final RequestLog requestLog = new RequestLog();
        requestLog.id = items[0];
        requestLog.urlPattern = items[1];
        requestLog.hitUrl = items[2];
        requestLog.ip = items[3];
        requestLog.method = items[4];
        requestLog.time = new Date(NumberUtils.toLong(items[5]));
        return requestLog;
    }

    public String toSerializerString(){
        return id+"|" + urlPattern +"|"+hitUrl+"|"+ip+"|"+method+"|"+time.getTime()+"";
    }
}

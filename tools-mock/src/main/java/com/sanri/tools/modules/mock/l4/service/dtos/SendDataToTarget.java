package com.sanri.tools.modules.mock.l4.service.dtos;

import lombok.Data;

@Data
public class SendDataToTarget {
    private SendData sendData;
    private String host;
    private int port;
}

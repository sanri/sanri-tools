package com.sanri.tools.modules.mock.l7.websocket.chat.configs;

import com.sanri.tools.modules.core.exception.ToolException;
import com.sanri.tools.modules.core.security.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * chat 需要开启系统登录才能使用
 */
@Aspect
@Component
public class SecurityCheckAspect {
    @Autowired(required = false)
    private UserService userService;

    @Pointcut("execution(public * com.sanri.tools.modules.mock.l7.websocket.chat.service.*Service.*(..))")
    public void pointcut(){}

    @Before("pointcut()")
    public void before(JoinPoint joinPoint){
        if (userService == null){
            throw new ToolException("需要开启 tools-security 模块才能使用此功能");
        }
    }
}

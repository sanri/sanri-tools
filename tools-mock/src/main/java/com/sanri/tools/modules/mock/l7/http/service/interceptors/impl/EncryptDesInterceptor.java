package com.sanri.tools.modules.mock.l7.http.service.interceptors.impl;


import com.sanri.tools.modules.mock.l7.http.service.dtos.ResponseConfig;
import com.sanri.tools.modules.mock.l7.http.service.interceptors.IntermediateData;
import com.sanri.tools.modules.mock.l7.http.service.interceptors.ResponseInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
@Order(1000)
public class EncryptDesInterceptor implements ResponseInterceptor {
    @Override
    public boolean match(HttpServletRequest request) {
        return false;
    }

    @Override
    public void interceptor(ResponseConfig responseConfig, IntermediateData intermediateData, Chain chain) {
        // 先看看是不是
        log.info("加密拦截器执行 des");
        chain.interceptor(responseConfig, intermediateData);
    }
}

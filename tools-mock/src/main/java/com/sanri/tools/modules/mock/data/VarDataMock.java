package com.sanri.tools.modules.mock.data;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sanri.tools.modules.core.service.file.FileManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.*;
import java.io.Reader;
import java.util.Iterator;

@Service
@Slf4j
public class VarDataMock {
    private ScriptEngineManager manager = new ScriptEngineManager();
    private ScriptEngine engine = manager.getEngineByName("js");

    /**
     * 脚本数据模拟
     * @param jsonObject
     * @return
     */
    public JSONObject mockScript(JSONObject jsonObject){
        if (jsonObject == null){
            return jsonObject;
        }
        final Iterator<String> iterator = jsonObject.keySet().iterator();
        while (iterator.hasNext()){
            final String next = iterator.next();
            final Object value = jsonObject.get(next);
            if (value == null){
                continue;
            }
            if (value instanceof JSONObject){
                mockScript((JSONObject) value);
                continue;
            }else if (value instanceof JSONArray){
                mockScript((JSONArray) value);
                continue;
            }else{
                final Object scriptOrigin = mockScriptOrigin(value);
                jsonObject.put(next,scriptOrigin);
            }
        }
        return jsonObject;
    }

    /**
     * 除 jsonobject jsonArray 数据的模拟
     * @param value
     * @return
     */
    public Object mockScriptOrigin(Object value){
        if (value instanceof String){
            // mock data
            String pattern = "";
            if (!((String) value).startsWith("@jsmock")){
                return value;
            }

            pattern = StringUtils.substring((String) value, 7);
            try {
                final Object eval = engine.eval(pattern);
                return eval;
            } catch (ScriptException e) {
                // 模拟失败, 则使用原值
                log.error("js 值模拟失败, 使用原值");
            }
        }
        // 其它类型使用原值
        return value;
    }

    /**
     * 对于数组数据的模拟
     * @param jsonArray
     * @return
     */
    public JSONArray mockScript(JSONArray jsonArray){
        if (jsonArray == null){
            return jsonArray;
        }
        JSONArray newJsonArray = new JSONArray();
        for(int i=0;i< jsonArray.size();i++){
            final Object object = jsonArray.get(i);
            if (object instanceof JSONObject){
                final JSONObject jsonObject = mockScript((JSONObject) object);
                newJsonArray.add(jsonObject);
            }else if (object instanceof JSONArray){
                final JSONArray mockArray = mockScript((JSONArray) object);
                newJsonArray.add(mockArray);
            }else {
                final Object o = mockScriptOrigin(object);
                newJsonArray.add(o);
            }
        }
        return newJsonArray;
    }

    /**
     * 执行脚本前加载通用上下文
     * 添加部分函数
     */
    public void initScriptContext(){
//        ScriptContext scriptContext = new SimpleScriptContext();
    }
}

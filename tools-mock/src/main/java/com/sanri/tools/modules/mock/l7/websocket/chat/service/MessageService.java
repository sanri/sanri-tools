//package com.sanri.tools.modules.mock.l7.websocket.chat.service;
//
//import com.sanri.tools.modules.core.security.UserOpenService;
//import com.sanri.tools.modules.core.security.entitys.UserProfile;
//import com.sanri.tools.modules.mock.l7.websocket.chat.domain.Message;
//import com.sanri.tools.modules.mock.l7.websocket.chat.repository.ChatGroupRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Service
//@Slf4j
//@Deprecated
//public class MessageService {
//
//    @Autowired
//    private ChatGroupService chatGroupService;
//
//    @Autowired
//    private ChatService chatService;
//
//    @Autowired(required = false)
//    private UserOpenService userOpenService;
//
//    String avatar = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAGsUlEQVRYR8WWaYxeVRnHf+ecu7zrdLYu0yndo0BJsSklVRqIS43LByMSIxQ1akigamrQukBEIRBsiFVQUxs0+kFjG5saBWIlLmw2BgwNUpVhsZTOlGln2nln5n3vfs4x932nnUKXd2o1nk/33nPPPb/7PP/nfx5hrbX8H4e4UAATN5B++T/+hfMG0Jkhkxm+9LBJDTP8A9TC25sA1oRYUqTsmDHQeQMkxqAO3YOYfQvj4z+lQx9E996HIwvof70Td/kfwRH/O4Agm8AOvodS3x60cnGEwloF2X70sV3I+XcjhfrvAthskqyxF7e6kswK0BoRDiCKS5AItASdDaDkemq1PVTMH0iMQ3X+lrYgM07B2NF7KDQGUX3fhsnduMXltALttPJPho7/jun6GPLAFtTSOxCiNXeuMWMAncLxQ/fSUVT4qg8r/dO+a/U4k3GCqFxMtetdCNFeC20BmjaRNEjjR6D6AZxXv4nBQThzQU5bSH5pkxGs1nDRBqyTgXgbjjp3FNoCGJ1iGMQe/ixp/y68F29CGo2RHlKVMVYgAaMjFDGpLuMs+zR27HGEdzWiZ+05I9EW4EScjZ0k1sdwnvs8SgZnSasklBm2/xsUut+Oc4Y0vXnhjAGyPLT1RwlHD1I6un1KgKd+zmCNIlz9JH7je+juj1MQ/e00yIwArNGER7Yg7QBKvJuRgw8wNxyFqWPEYjAiI5z7RZKqplP2kvA3SvPvp50OZwaQb2QnMDJBxgFxdJi0spzwt+tw5SDH1Qr63/sQ6vVtOPPuQOSiODnecHNaRNoC2DqIyvS6xuFf4qsXkOWbqdV+RUd5KdHw11DVDbheDdn7daRsOaFODFIJhDq1HA00ZdsaZwTQmUU5AqMhOnA1/uInOFFNZmIfurQC1/Egs9jGk2SVlThmFkIcQLMAlc8BY8N30tH1ZZRfbJmVDRGidX1OgGjwW8jyOmTnKnjxBrK+zfiVdYTpP/Amnkf1fvRkaTXGH6dUvQohW/VeH9mBMhF+77WkL23C7b0C0bORLEuI9n+S6qod7QHiwc2Y4mq8rg+jApesNEEuRDF6O7p8E3519VnVnUz8CSfcC12fInV78UhJdIoYvQ/lXobquf7MADp4DOlfBUpDMkoQHaKgyki3G4tFxEPY4iKMfp108i80an+ma/EDKKeHNAhxigKNT+PII1S6VpIEBymU+2jWa1Qjdbsx9hjKZIzXdmLUOubMu25aAyYeJRq5EzH7ZpzgaVxnBVYphMntVpJbsjHHwVtDffQ2ip2fwKte2ZzLdB09dDdpx0qUtxgvDBHFTqSVWGFa5Zp6JPZV0nSYgqyjem5tlugpIjRMvvJVSos+hxj+BZQundocjLDkhmuEQMbPIbuvRbuXoGRL0TlcWt9LUrmS8vB3sIVLaU5hQehmyA0KEQ1g5n4JZYOWlUunBZB3OenEDly1FDvyMJ6MwJ0Hws9XgmyVkZUJJjyMWnQX0eQ+3NI7WkaTvYRMhjByFnJkB8JbiJlac9IycyfVrxEU1pOKkFk9H0RKtwVgsuOER24lGHmNcmUhno4R1kNQBeU282hsiEoDrNQky36Ic3QrRhagewNp/ftQ+x3S24xf34OQpgUj/WZ08nuyEEzMuDufgtuD5jil2R86RQN5FOLfMz7wIL3UEVY3//5Ed2WtwMoUrEt62U78OMMWujEyT4LA1sfAHYZ/bkbggUhbmwuBRbfKNjfUt9xGMDJA6aIbmoAnNZC/HNV+TdLxfqrPrAXjtKL3JjMP0hpcvp14cj9d/Run/EBikgTjHoOn34egMKWB6YqzwiKFoHHFTopDP4b5dyEdMQ2go2PI2i5qjZ/jdN1CZd9XsLlwcuymnASRLhKuf5buQ/cTVlbhF65BlXyMMciRbUTBbuh/iMJTK9C5OKVuNqxOXgnGgbVPERz9LqXS5dhZa5D+smmAmAhpfQQxce1nZOXrCfeuoTs4jKMVY0s2Ul7xGYwJKcqLW8q2KUp6ZMYg8p4wL/mhzbBgK/XHLqEzG8WmgqQ0B3HNszQO3kjfwp1IKbFpgnALp58FcRagRrcR1J+g3P8gWfA8gX4GOV6l1CmIuq+jquZMxfaNB0v+MDt0L0q+Qr33RkpxBZvWSKTFSw5QD16gc8nWc1txyiiuyb2vq5n/bPw3xNHDVOb8CMwg9aO7qczfdFYrrkU/YVbhI2DLZGN/RaaPYnu/gMJl6OVNLHjr9vZnwalvWJuiiXBE9aybnm3CWoNOXsbxlzbb9xNV0fY0PO+dLmBB24bkAr49o6X/BixQY4s2k+q2AAAAAElFTkSuQmCC";
//
//    /**
//     * 转换为输出消息
//     * @param message
//     */
//    public OutMessage convertToOutMessage(Message message) throws IOException {
//        OutMessage outMessage = new OutMessage(message);
//        if (message.isGroup()) {
//            List<GroupUser> users = chatGroupService.users(message.getToContactId());
//            for (GroupUser user : users) {
//                if (message.getFromContactId().equals(user.getId())){
//                    outMessage.setFromUser(user);
//                    break;
//                }
//            }
//            outMessage.setToContactId(message.getToContactId());
//        }else {
//            UserProfile profile = userOpenService.profile(message.getFromContactId());
//            outMessage.setFromUser(profile);
//        }
//
//        if (outMessage.getFromUser() == null){
//            if ("system".equals(message.getFromContactId())){
//                // 如果是系统发出的, 则虚拟一个 profile
//                UserProfile userProfile = new UserProfile();
//                userProfile.setAvatar(avatar);
//                userProfile.setDisplayName("系统");
//                userProfile.setUsername("system");
//                outMessage.setFromUser(userProfile);
//            }
//        }
//        return outMessage;
//    }
//
//    /**
//     * 批量转换消息
//     * @param messages
//     * @return
//     * @throws IOException
//     */
//    public List<OutMessage> convertToOutMessage(List<Message> messages) throws IOException {
//        List<OutMessage> outMessages = new ArrayList<>();
//        // 后面优化
////        Map<String,List<UserProfile>> groupUserMap = new HashMap<>();
//        for (Message message : messages) {
//            outMessages.add(convertToOutMessage(message));
//        }
//        return outMessages;
//    }
//}

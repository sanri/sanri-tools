package com.sanri.tools.modules.mock.l7.websocket.chat.service;

import com.sanri.tools.modules.core.exception.ToolException;
import com.sanri.tools.modules.core.security.UserOpenService;
import com.sanri.tools.modules.core.security.UserService;
import com.sanri.tools.modules.core.security.entitys.UserProfile;
import com.sanri.tools.modules.mock.l7.websocket.chat.repository.ChatUserRepository;
import com.sanri.tools.modules.mock.l7.websocket.chat.repository.ChatGroupRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ChatGroupService {
    @Autowired
    private ChatGroupRepository chatGroupRepository;
    @Autowired
    private ChatUserRepository chatUserRepository;

    @Autowired(required = false)
    private UserService userService;

    @Autowired(required = false)
    private UserOpenService userOpenService;

    public void createGroup(UserProfile userProfile) throws IOException {
        boolean existGroup = chatGroupRepository.existGroup(userProfile.getUsername());
        if (existGroup){
            throw new ToolException("已经存在组 "+ userProfile.getUsername());
        }

        String admin = userService.username();

        chatGroupRepository.createGroup(userProfile, admin);

        chatUserRepository.joinGroup(admin, userProfile.getUsername());
    }

    public void dropGroup(String groupName) throws IOException {
        // 检测是否为管理员操作
        String username = userService.username();
        List<String> admins = chatGroupRepository.getAdmins(groupName);
        if (!admins.contains(username)){
            throw new ToolException("只有管理员可以操作解散组");
        }

        List<String> groupUsers = chatGroupRepository.groupUsers(groupName);

        // 每个用户先退出组
        for (String groupUser : groupUsers) {
            chatUserRepository.leaveGroup(groupUser, groupName);
        }

        // 删除组
        chatGroupRepository.removeGroup(groupName);
    }

    /**
     * 设置管理员列, 转让群时用
     * @param groupName
     * @param admins
     * @throws IOException
     */
    public void setAdmins(String groupName, List<String> admins) throws IOException {
        String username = userService.username();

        // 检查当前人是否有权限设置管理员, 只有超级管理员才能设置管理员
        List<String> groupAdmins = chatGroupRepository.getAdmins(groupName);
        if (!username.equals(groupAdmins.get(0))){
            throw new ToolException("只有超级管理员能设置管理员");
        }

        chatGroupRepository.setAdmins(groupName, admins);
    }


    public UserProfile profile(String groupName) throws IOException {
        return chatGroupRepository.profile(groupName);
    }

    public List<GroupUser> users(String groupName) throws IOException {
        List<GroupUser> users = new ArrayList<>();
        List<String> admins = chatGroupRepository.getAdmins(groupName);
        List<String> groupUsers = chatGroupRepository.groupUsers(groupName);
        for (String groupUser : groupUsers) {
            UserProfile profile = userOpenService.profile(groupUser);
            GroupUser user = new GroupUser(profile);
            if (admins.contains(groupUser)){
                user.setLevel(2);
                // 第 0 个管理员为超级管理员
                if (admins.get(0).equals(groupUser)){
                    user.setLevel(1);
                }
            }

            users.add(user);
        }
        return users;
    }

    public void addUser(String groupName, String username) throws IOException {
        // 检查用户是否存在
        boolean contains = chatUserRepository.users().contains(username);
        if (!contains){
            throw new ToolException("用户 "+ username + " 不存在");
        }
        boolean existGroup = chatGroupRepository.existGroup(groupName);
        if (!existGroup){
            throw new ToolException("不存在组 "+ groupName);
        }

        chatGroupRepository.addUser(groupName, username);

        chatUserRepository.joinGroup(username, groupName);
    }

    /**
     * 踢出用户
     * @param groupName
     * @param username
     * @throws IOException
     */
    public void removeUser(String groupName, String username) throws IOException {
        String loginUser = userService.username();
        List<String> admins = chatGroupRepository.getAdmins(groupName);
        if (admins.contains(loginUser)){
            throw new ToolException("管理员可使用踢人功能");
        }

        chatGroupRepository.removeUser(groupName, username);

        chatUserRepository.leaveGroup(username, groupName);
    }

    public List<UserProfile> groupList() throws IOException {
        List<UserProfile> groups = chatGroupRepository.groups();
        return groups;
    }
}

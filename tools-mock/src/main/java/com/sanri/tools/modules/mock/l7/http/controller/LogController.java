package com.sanri.tools.modules.mock.l7.http.controller;

import com.sanri.tools.modules.core.dtos.PageResponseDto;
import com.sanri.tools.modules.core.dtos.param.PageParam;
import com.sanri.tools.modules.mock.l7.http.service.LogService;
import com.sanri.tools.modules.mock.l7.http.service.dtos.RequestLog;
import com.sanri.tools.modules.mock.l7.http.service.dtos.RequestLogDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/mock/log")
public class LogController {
    @Autowired
    private LogService logService;

    /**
     * 排序列出日志
     * @param pageParam
     * @return
     * @throws IOException
     */
    @GetMapping("/list")
    public PageResponseDto<List<RequestLog>> list(@Validated PageParam pageParam, String urlPattern) throws IOException {
        return logService.list(pageParam.getPageNo(),pageParam.getPageSize(),urlPattern);
    }

    /**
     * 日志详情
     * @param id
     * @return
     * @throws IOException
     */
    @GetMapping("/detail")
    public RequestLogDto detail(@NotBlank String id) throws IOException {
        return logService.detail(id);
    }
}

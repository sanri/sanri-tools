package jdbctest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class Base64ToImage {
    public static void main(String[] args) throws IOException {
        String origin = FileUtils.readFileToString(new File("d:/test/base64.txt"));
        origin = origin.substring(origin.indexOf(',') + 1);
        byte[] bytes1 = Base64.decodeBase64(origin);
        File file = new File("d:/test/image.jpg");
        FileUtils.writeByteArrayToFile(file, bytes1);
    }
}

package com.sanri.tools.modules.versioncontrol.project;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.type.Type;
import com.sanri.tools.modules.versioncontrol.dtos.ProjectLocation;
import com.sanri.tools.modules.versioncontrol.git.GitRepositoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * java 文件分析
 */
@Service
@Slf4j
public class JavaFileParseService {

    @Autowired
    private GitRepositoryService gitRepositoryService;

    /**
     * 单个文件的解析
     * @param projectLocation
     * @param filePath
     * @return
     */
    public CompilationUnit parseProjectFile(ProjectLocation projectLocation, String filePath) throws FileNotFoundException {
        final File projectDir = gitRepositoryService.loadProjectDir(projectLocation);
        final File file = new File(projectDir, filePath);
        final CompilationUnit parse = StaticJavaParser.parse(file);
        return parse;
    }

    /**
     * 解析类文件引用的所有类, 需要指定包
     * @param projectLocation 项目路径标识
     * @param filePath 文件路径 - 相对于项目的绝对路径
     * @param packagePrefix 包前缀
     */
    public void parseRefrenceClass(ProjectLocation projectLocation, String filePath, String packagePrefix) throws FileNotFoundException {
        final File projectDir = gitRepositoryService.loadProjectDir(projectLocation);
        final File file = new File(projectDir, filePath);

    }
}

package com.sanri.tools.modules.minio.service.dtos;

import lombok.Data;

@Data
public class MinioConnect {
    private String endpoint;
    private String accessKey;
    private String secretKey;
}

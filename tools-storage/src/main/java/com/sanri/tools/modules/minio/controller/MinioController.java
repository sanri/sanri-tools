package com.sanri.tools.modules.minio.controller;

import com.sanri.tools.modules.core.dtos.param.PageParam;
import com.sanri.tools.modules.minio.service.MinioService;
import com.sanri.tools.modules.minio.service.dtos.Minio;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * fastdfs 相关功能
 *
 * @author sanri
 */
@RestController
@Slf4j
@RequestMapping("/minio")
@Validated
public class MinioController {

    @Autowired
    private MinioService minioService;

    /**
     * 存储桶列表
     *
     * @param connName
     * @return
     * @throws Exception
     */
    @GetMapping("/buckets")
    public List<Minio.Bucket> buckets(@NotBlank String connName) throws Exception {
        return minioService.buckets(connName);
    }

    /**
     * 搜索图片
     *
     * @param connName
     * @param bucketName
     * @param prefix
     * @param pageParam
     * @return
     * @throws Exception
     */
    @GetMapping("/searchImages")
    public List<Minio.ImageInfo> searchImages(@NotBlank String connName, @NotBlank String bucketName, String prefix, PageParam pageParam) throws Exception {
        if (StringUtils.isBlank(prefix)) {
            prefix = "/";
        }
        return minioService.searchImages(connName, bucketName, prefix, pageParam);
    }

    /**
     * 单个文件信息查询
     * @param connName
     * @param bucketName
     * @param objectName
     * @return
     * @throws Exception
     */
    @GetMapping("/fileInfo")
    public Minio.ImageInfo fileInfo(@NotBlank String connName, @NotBlank String bucketName, @NotBlank String objectName) throws Exception {
        return minioService.fileInfo(connName, bucketName, objectName);
    }

}

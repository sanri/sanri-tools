package com.sanri.tools.modules.minio.service.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.minio.ObjectStat;
import io.minio.messages.Item;
import io.minio.messages.NotificationConfiguration;
import lombok.Data;

import java.util.Date;

/**
 * minio 相关实体对象
 */
public class Minio {

    @Data
    public static final class ImageInfo{
        private String objectName;
        private String etag;
        private long size;
        private String owner;
        private boolean dir;
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
        private Date createDate;

        public long getCreateTimestamp(){
            if (createDate != null){
                return createDate.getTime();
            }
            return 0L;
        }
    }

    @Data
    public static final class Bucket {
        private String name;
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
        private Date createDate;

        private String bucketPolicy;
        private String bucketLifeCycle;
        private NotificationConfiguration notificationConfiguration;

        public Bucket() {
        }

    }

    public static final ImageInfo convertToImageInfo(Item item){
        ImageInfo imageInfo = new ImageInfo();
        if (!item.isDir()) {
            imageInfo.createDate = Date.from(item.lastModified().toInstant());
        }
        imageInfo.dir = item.isDir();
        imageInfo.etag = item.etag();
        imageInfo.objectName = item.objectName();
        if (item.owner() != null) {
            imageInfo.owner = item.owner().displayName();
        }
        imageInfo.size = item.size();
        return imageInfo;
    }

    public static final ImageInfo convertToImageInfo(ObjectStat objectStat){
        ImageInfo imageInfo = new ImageInfo();
        imageInfo.createDate = Date.from(objectStat.createdTime().toInstant());
        imageInfo.dir = false;
        imageInfo.etag = objectStat.etag();
        imageInfo.objectName = objectStat.name();
        imageInfo.size = objectStat.length();
        return imageInfo;
    }
}

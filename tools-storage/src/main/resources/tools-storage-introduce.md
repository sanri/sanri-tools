# fastdfs
因为 fastdfs 没有可视化界面, 所以添加了此工具

## 预览页
可以通过输入 dfsId 图片路径来获取图片, 使用换行符来分隔多张图, 可以点击下一张来进行多张图的预览

下载按扭可以将图片进行批量下载下来

## 上传
不建议在这里上传过大的文件, 小图片可以在这里上传进行上传下载的测试

# minio

## 预览页
和 fastdfs 预览功能一致，不过唯一键换成了 objectName

## 文件搜索
可以按照 objectName 目录进行搜索， 点击行进入目录， 到文件一级时， 可以直接预览文件

## 文件上传
和 fastdfs 一样， 不建议在这里上传大文件， 小文件可用于测试文件服务器功能是否正常
package sys;

import io.minio.MinioClient;
import io.minio.Result;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import io.minio.messages.Item;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.Iterator;

public class MinioTests {

    @Test
    public void test1() throws Exception {
        MinioClient minioClient = new MinioClient("http://192.168.17.13:9990","aeye","Zhy778899");
        System.out.println(minioClient);
        String objectName = "ZHY/202208/capture/7c3556ffbe024685a630fa9bc5ab69f2_null/29/background@@03cb3fb8-0770-461d-9b96-076803d1754b.jpg";
        try(InputStream minio = minioClient.getObject("group1", objectName);){
            FileUtils.copyInputStreamToFile(minio, new File("d:/test/xx.png"));
        }

    }

    @Test
    public void test2() throws Exception{
        MinioClient minioClient = new MinioClient("http://192.168.17.13:9990","aeye","Zhy778899");
        Iterable<Result<Item>> iterable = minioClient.listObjects("group1", "/",false, true);
        Iterator<Result<Item>> iterator = iterable.iterator();
        while (iterator.hasNext()){
            Result<Item> next = iterator.next();
            Item item = next.get();
            System.out.println(item.objectName());
        }
    }
}

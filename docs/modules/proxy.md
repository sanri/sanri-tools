# 代理模块介绍

这个模块主要是借鉴于 idea 的 restclient, 用于前端的代理 http 请求

## http 配置说明

> 配置位置: 系统管理 -> 连接管理 -> httprequest 模块 -> 新建

如果你了解 idea 的 restclient, 可以不用看这个说明, 因为这个是直接借鉴于 idea 

\### 表示一个请求的说明, 同时也是分隔多个请求的分隔符, 可以在旁边写上这个请求的注释

\# 一个 # 表示注释

第一行是请求行 GET/POST/PUT/DELETE schema://host:port/path?queryString

紧挨请求行的是请求头, key: value 格式 

空一行后, 可以加请求体, 根据内容格式写入请求体

可以使用变量, 供后面调用者传参, 变量格式为 {{var}}

\< {% %} 中包含的是处理脚本, 可用变量为 response, client ; 变量和 rest client 一样的, 使用者可以先在 rest client 中调试, 然后把代码搬进来即可

> 本功能不支持文件处理, 只适用于接口数据调用
 
## httpbind 扩展
此功能主要为了有些接口是需要登录才能使用的, 所以在请求真实接口前, 先请求登录接口, 获取对应参数后, 再将参数带到下一个接口中进行请求

在纯 http 的管理端, 如 elasticsearch,influxdb, 微服务之类的接口, 可以使用绑定来定制管理界面

### 配置说明

> 配置位置 系统管理 -> 通用配置项 -> httpbind 模块 -> 新建

```
# 绑定配置文件, 格式  key:<httpfile>:#<req>:请求说明:[认证]
# 如果有认证, 第一个绑定需要是认证请求, 只要认证列有值即可
login:sanritools:#1:登录:auth
plugins:sanritools:#2:插件列表
```

## pageconfig 扩展(暂未实现)
可用于页面管理, 管理页面中的部件绘制, 每个部件使用的 http 模板配置和参数来源

这功能是想通过配置来画出一个界面，而不需要写任何代码， 目前事件不好处理，有研究过动态事件的可以和作者聊一下，只要动态事件可处理后，此功能就可以完善了

1. 页面部件配置
2. 页面进入时, 先加载配置信息, 然后渲染部件, 通过参数加载数据
3. 当页面组件有事件时, 可指定打开配置的哪个组件, 然后重复上述方式加载部件
4. 所有部件以面板方式展现, 可以进行移动和隐藏

### 配置说明 

```text
{
  "name": "test",      配置页面名
  "enable": true,      是否启用
  "pageComponents": [  页面组件
    {
      "name": "module1", 组件名
      "sequence": 0,     组件加载顺序, 启动顺序为 0 的组件会首先加载 
      "data": "httpConfig|test.http&#1", 数据获取 schema|schema对应的内容格式; 
目前支持 httpconfig(http配置&reqestId) 和 static(直接写内容) 
      "render": "xxComponent", 使用的前端组件
      "enable": true 是否启用
    }
  ]
}
```


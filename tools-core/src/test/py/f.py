import numpy as np
from numpy import *;

def sum(*args):
    val = 0
    for item in args:
        val += item
    return val

def test2():
    arr = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    print(arr.ndim)

def test5():
    the_array = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

    filter_arr = np.logical_or(the_array < 3, the_array == 4)
    print(the_array[filter_arr])


if __name__ == 'main':
    sval = sum(1,2,3,4)

    print(sval)

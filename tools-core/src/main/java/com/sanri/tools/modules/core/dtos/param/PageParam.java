package com.sanri.tools.modules.core.dtos.param;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.math.NumberUtils;

@Setter
@ToString
public class PageParam {
	/**
	 * 页号
	 */
	private String pageNo;
	/**
	 * 页大小
	 */
	private String pageSize;

	public Integer getPageNo() {
		return NumberUtils.toInt(pageNo,1);
	}

	public Integer getPageSize() {
		return NumberUtils.toInt(pageSize,10);
	}

	@JSONField(serialize = false)
	public Integer getStart(){
		int pageNo = NumberUtils.toInt(this.pageNo, 1);
		int pageSize = NumberUtils.toInt(this.pageSize, 10);
		return (pageNo - 1) * pageSize;
	}

	@JSONField(serialize = false)
	public Integer getEnd(){
		int pageNo = NumberUtils.toInt(this.pageNo, 1);
		int pageSize = NumberUtils.toInt(this.pageSize, 10);
		return (pageNo) * pageSize;
	}
}

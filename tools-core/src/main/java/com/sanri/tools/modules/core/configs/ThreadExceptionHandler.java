package com.sanri.tools.modules.core.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {

    public ThreadExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        log.error("线程[{}]抛出异常未被捕获: ",t.getName(),e.getMessage(),e);
    }
}

package com.sanri.tools.modules.core.service.cc.dtos;

import lombok.Data;

/**
 * 通用简单配置维护
 */
@Data
public class CommonConfigDto {
    /**
     * 配置模板名
     */
    private String template;
    /**
     * 基础名
     */
    private String baseName;

    /**
     * 扩展后缀名
     */
    private String extension;
}

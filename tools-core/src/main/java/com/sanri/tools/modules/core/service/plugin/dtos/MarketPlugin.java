package com.sanri.tools.modules.core.service.plugin.dtos;

import com.sanri.tools.modules.core.utils.OnlyPath;
import com.sanri.tools.modules.core.utils.Version;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Data
@Slf4j
public class MarketPlugin {
    @NotBlank
    private String id;
    @NotBlank
    private String name;
    @NotBlank
    private String author;
    private String desc;
    private List<MarketPlugin> dependencies = new ArrayList<>();
    @NotBlank
    private String downloadAddress;
    @NotBlank
    private String version;
    /**
     * 下载后的文件名
     */
    private String filename;
    /**
     * 是否已经安装
     */
    private boolean loaded;

    public MarketPlugin() {
    }

    public MarketPlugin(String id, String name, String author, String desc, String downloadAddress, String version) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.desc = desc;
        this.downloadAddress = downloadAddress;
        this.version = version;
    }

    public String getFilename() {
        if (StringUtils.isNotBlank(filename)) {
            return filename;
        }

        // 如果没有提供文件名, 则解析下载地址得到文件名
        return downloadAddress.hashCode() + "";
    }
}

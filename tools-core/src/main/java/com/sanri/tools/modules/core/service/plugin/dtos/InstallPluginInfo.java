package com.sanri.tools.modules.core.service.plugin.dtos;

import com.sanri.tools.modules.core.utils.Version;
import lombok.Data;

@Data
public class InstallPluginInfo {
    private String downloadAddress;
    private String pluginId;
    private Version version;
    private String filename;
    private DependencyPath dependencyPath;

    public InstallPluginInfo(MarketPlugin marketPlugin) {
        this.pluginId = marketPlugin.getId();
        this.version = new Version(marketPlugin.getVersion());
        this.downloadAddress = marketPlugin.getDownloadAddress();
        this.filename = marketPlugin.getFilename();
    }

    @Data
    public static final class DependencyPath{
        private String name;
        private DependencyPath prev;

        public DependencyPath(String name) {
            this.name = name;
        }

        public DependencyPath(String pluginId, Version version) {
            this.name = pluginId + ":" + version;
        }

        /**
         * 路径长度
         * @return
         */
        public int length(){
            int length = 1;
            DependencyPath current = this;
            while (current.prev != null){
                length ++;
                current = current.prev;
            }
            return length;
        }
    }
}

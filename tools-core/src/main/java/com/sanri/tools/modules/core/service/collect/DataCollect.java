package com.sanri.tools.modules.core.service.collect;

import org.springframework.scheduling.annotation.Async;

public interface DataCollect {

    @Async
    void collect();
}

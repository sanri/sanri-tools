package com.sanri.tools.modules.core.service.plugin.events;

import org.springframework.context.ApplicationEvent;

public class ModuleChangeEvent extends ApplicationEvent {
    private boolean install;
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public ModuleChangeEvent(Object source, boolean install) {
        super(source);
        this.install = install;
    }

    public boolean isInstall() {
        return install;
    }

}

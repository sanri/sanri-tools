package com.sanri.tools.modules.core.service.connect;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface ActiveConnectManage {

    /**
     * 当前模块
     * @return
     */
    String module();

    /**
     * 激活连接列表
     * @return
     */
    Set<String> activeConnects();

    /**
     * 连接实体类
     * @return
     */
    Class<?> connectClass();

    /**
     * 杀掉一个连接
     * @param id
     * @return
     */
    boolean kill(String id);

    /**
     * 重连
     * @param id
     */
    void reconnect(String id) throws Exception;
}

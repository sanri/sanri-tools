package com.sanri.tools.modules.core.controller;

import com.sanri.tools.modules.core.service.cc.CommonConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.io.IOException;

/**
 * 通用配置项
 * @author sanri
 */
@RestController
@RequestMapping("/cc")
@Validated
public class CommonConfigController {

    @Autowired
    private CommonConfigService commonConfigService;

    /**
     * 模板列表
     * @return
     */
    @GetMapping("/templates")
    public String [] templates(){
        return commonConfigService.templateNames();
    }

    /**
     * 模板文件列表
     * @param templateName 模板名
     * @return
     */
    @GetMapping("/{templateName}/files")
    public String[] templateFiles(@PathVariable("templateName") @NotBlank String templateName){
        return commonConfigService.listTemplateFileNames(templateName);
    }

    /**
     * 读取文件内容
     * @param templateName 模板名
     * @param baseName 基础名
     * @return
     * @throws IOException
     */
    @GetMapping("/{templateName}/{baseName}/readContent")
    public String readFileContent(@PathVariable("templateName") @NotBlank String templateName,
                                  @PathVariable("baseName") @NotBlank String baseName) throws IOException {
        return commonConfigService.readTemplateFileContent(templateName, baseName);
    }

    /**
     * 写入文件内容
     * @param templateName 模板名
     * @param baseName 基础名
     * @param content 文件内容
     * @throws IOException
     */
    @PostMapping("/{templateName}/{baseName}/writeContent")
    public void writeFileContent(@PathVariable("templateName") @NotBlank String templateName,
                                 @PathVariable("baseName") @NotBlank String baseName, @RequestBody String content) throws IOException {
        commonConfigService.editFile(templateName, baseName, content);
    }

    /**
     * 删除一个模板文件
     * @param templateName
     * @param baseName
     */
    @PostMapping("/{templateName}/{baseName}/drop")
    public void dropTemplateFile(@PathVariable("templateName") @NotBlank String templateName,
                                 @PathVariable("baseName") @NotBlank String baseName){
        commonConfigService.dropConfigFile(templateName, baseName);
    }

    /**
     * 模板示例
     * @param templateName
     * @return
     */
    @GetMapping("/{templateName}/example")
    public String templateExample(@PathVariable("templateName") @NotBlank String templateName) throws IOException {
        return commonConfigService.templateExample(templateName);
    }
}

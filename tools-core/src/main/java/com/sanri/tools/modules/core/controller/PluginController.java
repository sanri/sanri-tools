package com.sanri.tools.modules.core.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.sanri.tools.modules.core.service.plugin.MarketPluginCacheData;
import com.sanri.tools.modules.core.service.plugin.PluginManager;
import com.sanri.tools.modules.core.service.plugin.PluginRepoManager;
import com.sanri.tools.modules.core.service.plugin.dtos.EnhancePlugin;
import com.sanri.tools.modules.core.service.plugin.dtos.MarketPlugin;
import com.sanri.tools.modules.core.service.plugin.dtos.PluginWithHelpContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 插件管理
 * @author sanri
 */
@RestController
@RequestMapping("/plugin")
@Validated
public class PluginController {
    @Autowired
    private PluginManager pluginManager;
    @Autowired
    private MarketPluginCacheData marketPluginCacheData;

    /**
     * 获取插件列表,经过排序的
     * @return
     */
    @GetMapping("/list")
    public List<EnhancePlugin> plugins() throws IOException {
        return pluginManager.localPlugins();
    }

    /**
     * 访问某个插件
     * @param pluginId 插件标识
     */
    @GetMapping("/visited")
    public void visited(@NotNull String pluginId){
        pluginManager.visitedPlugin(pluginId);
    }

    /**
     * 获取插件详情
     * @param pluginId 插件标识
     * @return
     */
    @GetMapping("/detail")
    public PluginWithHelpContent detail(@NotNull String pluginId) throws IOException {
        return pluginManager.detailWithHelpContent(pluginId);
    }

    /**
     * 将当前访问量和访问次数进行序列化
     */
    @GetMapping("/serializer")
    public void serializer(){
        pluginManager.serializer();
    }

    /**
     * 可安装插件搜索
     * @param keyword
     * @return
     */
    @GetMapping("/marketPlacePlugins")
    public List<MarketPlugin> marketPlacePlugins(String keyword){
        return marketPluginCacheData.search(keyword);
    }

    @Autowired
    private PluginRepoManager pluginRepoManager;

    /**
     * 仓库列表
     * @return
     */
    @GetMapping("/repos")
    public Set<String> repos(){
        return pluginRepoManager.list();
    }

    /**
     * 添加仓库
     * @param repo
     */
    @PostMapping("/repo/add")
    public void repoAdd(@NotBlank String repo) throws Exception {
        pluginRepoManager.add(repo);

        // 仓库变更后, 删除缓存
        marketPluginCacheData.forceCleanCache();
    }

    /**
     * 删除仓库
     * @param repo
     */
    @PostMapping("/repo/remove")
    public void repoDel(@NotBlank String repo) throws Exception {
        pluginRepoManager.remove(repo);

        // 仓库变更后, 删除缓存
        marketPluginCacheData.forceCleanCache();
    }

    /**
     * 插件安装
     * @param marketPlugin
     * @throws IOException
     */
    @PostMapping("/install")
    public void install(@RequestBody MarketPlugin marketPlugin) throws IOException {
        marketPluginCacheData.install(marketPlugin);
    }

    /**
     * 插件卸载
     * @param pluginId
     */
    @PostMapping("/uninstall")
    public void uninstall(@NotBlank String pluginId) throws IOException {
        marketPluginCacheData.uninstall(pluginId);
    }
}

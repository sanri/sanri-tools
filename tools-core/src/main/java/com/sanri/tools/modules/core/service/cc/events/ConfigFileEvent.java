package com.sanri.tools.modules.core.service.cc.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * 通用配置删除事件
 */
@Data
public class ConfigFileEvent extends ApplicationEvent {
    private String templateName;
    private String baseName;
    private Action action;

    public ConfigFileEvent(Object source) {
        super(source);
    }

    public enum Action{
        DELETE, EDIT
    }
}

package com.sanri.tools.modules.core.security;

import com.sanri.tools.modules.core.security.entitys.UserProfile;

import java.io.IOException;

/**
 * 获取用户公开信息
 */
public interface UserOpenService {

    /**
     * 获取用户属性
     * @param username 用户名
     * @return
     */
    UserProfile profile(String username) throws IOException;
}

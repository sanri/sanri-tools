package com.sanri.tools.modules.core.security.entitys;

import lombok.Data;

@Data
public class UserProfile {
    private String username;
    /**
     * 昵称
     */
    private String displayName;
    /**
     * 头像
     */
    private String avatar;

    public UserProfile(String username) {
        this.username = username;
    }

    public UserProfile() {
    }

    public String getId(){
        return username;
    }
    public void setId(String id){
        this.username = id;
    }
}

package com.sanri.tools.modules.core.dtos.param;

import lombok.Data;

@Data
public class RedisConnectParam extends AbstractConnectParam {
    private AuthParam authParam = new AuthParam();
    /**
     * 哨兵节点密码
     */
    private String sentinelPassword;
}

package com.sanri.tools.maven.service.dtos;

import lombok.Data;
import org.apache.commons.collections.CollectionUtils;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.util.graph.visitor.PreorderNodeListGenerator;

import java.io.File;
import java.util.Collection;
import java.util.List;

@Data
public class JarCollect {
    private transient PreorderNodeListGenerator preorderNodeListGenerator;

    public JarCollect() {
    }

    public JarCollect(PreorderNodeListGenerator preorderNodeListGenerator) {
        this.preorderNodeListGenerator = preorderNodeListGenerator;
    }

    /**
     * lombok 单独处理
     * @return 如果有 lombok , 则返回路径, 否则返回 null
     */
    public File findLombok(){
        final List<File> files = preorderNodeListGenerator.getFiles();
        if (CollectionUtils.isNotEmpty(files)) {
            for (File file : files) {
                if (file.getName().contains("lombok")){
                    return file;
                }
            }
        }
        return null;
    }

    public PreorderNodeListGenerator getPreorderNodeListGenerator() {
        return preorderNodeListGenerator;
    }

    public Collection<File> getFiles(){
        return preorderNodeListGenerator.getFiles();
    }

    public String getClasspath(){
        return preorderNodeListGenerator.getClassPath();
    }

    public List<Artifact> getArtifacts(){
        return preorderNodeListGenerator.getArtifacts(false);
    }
}

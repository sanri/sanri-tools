package com.sanri.tools.modules.page.service.dto.pageconfig;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huangzhengri
 */
@Data
public class PageConfig {
    private boolean enable = true;
    private List<PageComponent> pageComponents = new ArrayList<>();
}
package com.sanri.tools.modules.page.controller;

import com.sanri.tools.modules.page.service.ComponentService;
import com.sanri.tools.modules.page.service.dto.LoadDataParam;
import com.sanri.tools.modules.page.service.dto.pageconfig.PageComponent;
import com.sanri.tools.modules.page.service.dto.pageconfig.PageConfig;
import com.sanri.tools.modules.page.service.PageManager;
import com.sanri.tools.modules.proxy.service.HttpFileManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequestMapping("/pageconfig")
@Validated
public class PageController {

    @Autowired
    private PageManager pageManager;

    @Autowired
    private ComponentService componentService;

    /**
     * 模块加载数据
     */
    @PostMapping("/component/loadData")
    public void loadData(@RequestBody LoadDataParam loadDataParam, HttpServletResponse response) throws IOException {
        componentService.loadData(loadDataParam, response);
    }

    /**
     * 重新注入动态菜单
     */
    @PostMapping("/reInjectResourceAndMenus")
    public void reInjectResourceAndMenus() throws IOException {
        pageManager.reInjectResourceAndMenus();
    }
}

package com.sanri.tools.modules.proxy.service.sh;

import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.entity.ContentType;

public interface RestClientResponse {

    ContentType getContentType();

    StatusLine getStatusLine();

    Header[] getAllHeaders();


    String getContent();

    int getContentLength();
}


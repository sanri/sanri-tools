package com.sanri.tools.modules.proxy.service.sh;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class HttpClientExternal {
    private static final String HTTP_CLIENT = "HTTP Client";
    public HttpClientVariables global = new HttpClientVariables();

    public static class HttpClientVariables {
        private final ConcurrentMap<String, String> myVariables = new ConcurrentHashMap<>();

        public boolean isEmpty() {
            return this.myVariables.isEmpty();
        }
        public void set( String name,  String value) {
            if (name == null) throw new RuntimeException("name is null");
            if (value == null) throw new RuntimeException("value is null");
            this.myVariables.put(name, value);
        }
        public String get( String name) {
            if (name == null) throw new RuntimeException("name is null");
            return this.myVariables.get(name);
        }
        public Map<String, String> getAllVars() {
            return this.myVariables;
        }
        public void clear( String name) {
            if (name == null) throw new RuntimeException("name is null");
            this.myVariables.remove(name);
        }
        public void clearAll() {
            this.myVariables.clear();
        }

    }
}

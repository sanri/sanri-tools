package com.sanri.tools.modules.proxy.service.sh;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HttpClientSession {
    public HttpClientExternal client;
    public HttpResponseHeaders headers;
    public HttpResponseContentType contentType;
    public String content;
    public int statusCode;

    public HttpClientSession(HttpClientExternal client, HttpResponseHeaders headers, HttpResponseContentType contentType, String content, int statusCode) {
        this.client = client;
        this.headers = headers;
        this.contentType = contentType;
        this.content = content;
        this.statusCode = statusCode;
    }

    public static HttpClientSession create(RestClientResponse response) {
        if (response == null) throw new RuntimeException("没有响应信息");
        HttpClientExternal client = new HttpClientExternal();
        HttpResponseHeaders headers = new HttpResponseHeaders(response.getAllHeaders());
        ContentType type = response.getContentType();
        Charset charset = type.getCharset();
        HttpResponseContentType contentType = new HttpResponseContentType(type.getMimeType(), (charset != null) ? charset.name() : "");
        return new HttpClientSession(client, headers, contentType, response.getContent(), response.getStatusLine().getStatusCode());
    }

    public static class HttpResponseContentType {
        public String mimeType;
        public String charset;

        public HttpResponseContentType(String mimeType, String charset) {
            this.mimeType = mimeType;
            this.charset = charset;
        }

        public String toString() {
            return this.mimeType + "; charset=" + this.charset;
        }
    }

    public static class HttpResponseHeaders {
        private static final String EMPTY_HEADERS = "Empty";
        public Header[] headers;

        public HttpResponseHeaders(Header[] headers) {
            this.headers = headers;
        }

        public String valueOf(String name) {
            if (name == null) throw new RuntimeException("name is null");
            for (Header header : this.headers) {
                if (StringUtils.equalsIgnoreCase(header.getName(), name)) {
                    return header.getValue();
                }
            }
            return null;
        }

        public List<String> valuesOf(String name) {
            if (name == null) throw new RuntimeException("name is null");
            List<String> values = new ArrayList<>();
            for (Header header : this.headers) {
                if (StringUtils.equalsIgnoreCase(header.getName(), name)) {
                    values.add(header.getValue());
                }
            }
            return values;
        }

        public String toString() {
            if (this.headers.length == 0) {
                return "Empty";
            }
            return Stream.<Header>of(this.headers).map(header -> header.getName() + ": " + header.getValue()).collect(Collectors.joining("\n"));
        }

    }
}

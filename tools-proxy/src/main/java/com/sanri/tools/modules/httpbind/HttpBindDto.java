package com.sanri.tools.modules.httpbind;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class HttpBindDto {
    private String key;
    /**
     * http 连接名称, 即配置 http 请示文件的名称
     */
    private String httpConnName;
    /**
     * 第几个请求 例: #3
     */
    private String reqId;
    /**
     * 请求说明
     */
    private String comment;

    /**
     * 只有有值, 就标记为认证请求
     */
    private String authorization;

    public HttpBindDto() {
    }

    public HttpBindDto(String key, String httpConnName, String reqId, String comment) {
        this.key = key;
        this.httpConnName = httpConnName;
        this.reqId = reqId;
        this.comment = comment;
    }

    /**
     * 判断当前请求是否为认证请求
     * @return
     */
    public boolean isAuthorization(){
        return StringUtils.isNotBlank(StringUtils.trim(authorization));
    }
}

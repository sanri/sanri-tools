package com.sanri.tools.modules.page.service.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class LoadDataParam {
    private String pageName;
    private String componentName;
    private Map<String,String> params = new HashMap<>();
}

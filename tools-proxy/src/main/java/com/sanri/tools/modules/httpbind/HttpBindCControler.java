package com.sanri.tools.modules.httpbind;

import com.sanri.tools.modules.core.exception.ToolException;
import com.sanri.tools.modules.proxy.service.HttpFileManager;
import com.sanri.tools.modules.proxy.service.ProxyHttpService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/httpbind")
@Validated
@Slf4j
public class HttpBindCControler {

    @Autowired
    private HttpBindCCService httpBindCCService;

    /**
     * 配置列表
     * @param configFileName
     * @return
     * @throws IOException
     */
    @GetMapping("/configs")
    public Map<String,HttpBindDto> configs(@NotBlank String configFileName) throws IOException {
        return httpBindCCService.configs(configFileName);
    }

    /**
     * 代理请求; 绑定 http 请求文件中的 reqId 形式
     * 主要作用是解决了需要登录的问题
     * @param proxyBindHttpParam
     */
    @PostMapping("/proxy")
    public void proxy(@RequestBody ProxyBindHttpParam proxyBindHttpParam, HttpServletRequest request, HttpServletResponse response) throws IOException {
       httpBindCCService.proxy(proxyBindHttpParam,response);
    }

}

package com.sanri.tools.modules.proxy.service.sh;


import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;

public class RestClientResponseImpl implements RestClientResponse {
    private final CloseableHttpResponse httpResponse;
    private final String content;

    private RestClientResponseImpl(CloseableHttpResponse response, String content) {
        this.httpResponse = response;
        this.content = content;
    }


    public static RestClientResponse createResponse(CloseableHttpResponse response, String contentBuffer) {
        if (response == null) throw new RuntimeException("异常" + 2);
        if (contentBuffer == null) throw new RuntimeException("异常" + 3);
        return new RestClientResponseImpl(response, contentBuffer);
    }


    public static RestClientResponse createEmptyResponse(CloseableHttpResponse response) {
        if (response == null) throw new RuntimeException("异常" + 4);
        return new RestClientResponseImpl(response, "");
    }


    public ContentType getContentType() {
        if (ContentType.getOrDefault(this.httpResponse.getEntity()) == null)
            throw new RuntimeException("异常" + 5);
        return ContentType.getOrDefault(this.httpResponse.getEntity());
    }


    public StatusLine getStatusLine() {
        if (this.httpResponse.getStatusLine() == null) throw new RuntimeException("异常" + 6);
        return this.httpResponse.getStatusLine();
    }


    public Header[] getAllHeaders() {
        if (this.httpResponse.getAllHeaders() == null) throw new RuntimeException("异常" + 7);
        return this.httpResponse.getAllHeaders();
    }


    public String getContent() {
        if (this.content == null) throw new RuntimeException("异常" + 8);
        return this.content;
    }


    public int getContentLength() {
        return this.content.length();
    }
}

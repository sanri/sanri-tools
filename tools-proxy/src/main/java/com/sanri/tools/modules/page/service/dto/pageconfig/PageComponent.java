package com.sanri.tools.modules.page.service.dto.pageconfig;

import lombok.Data;

@Data
public class PageComponent{
    /**
     * 名称
     */
    private String name;
    /**
     * 渲染的前端组件
     */
    private String render;
    /**
     * 是否启用
     */
    private boolean enable = true;
    /**
     * 出场顺序, 出场顺序为 0 的首先出现,
     * 不为 0 的代表是由别的组件调起
     */
    private int sequence;
    /**
     * 数据来源配置 例: http(eureka#2)
     * 静态数据: static|直接写数据
     * 动态数据: httpConfig|connName&reqId
     */
    private String data;

    /**
     * 数据处理;处理后的数据要能和组件适配
     */
    private String dataHandler;
}
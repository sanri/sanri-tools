package com.sanri.tools.modules.httpbind;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

@Data
public class ProxyBindHttpParam {
    /**
     * http绑定key
     */
    @NotBlank
    private String key;
    /**
     * 配置文件名 http 绑定的配置文件名
     */
    @NotBlank
    private String configFileName;

    /**
     * 所有 http 文件中的变量参数信息
     */
    private Map<String,String> params = new HashMap<>();
}

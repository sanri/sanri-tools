package com.sanri.tools.modules.proxy;

import com.sanri.tools.modules.core.utils.PoolHttpClient;

import com.sanri.tools.modules.proxy.service.HttpFileParse;
import com.sanri.tools.modules.proxy.service.dtos.RequestInfo;
import com.sanri.tools.modules.proxy.service.sh.HttpClientSession;
import com.sanri.tools.modules.proxy.service.sh.RestClientResponse;
import com.sanri.tools.modules.proxy.service.sh.RestClientResponseImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class HttpFileParseTests {

    @Test
    public void test1() throws IOException {
        final File file = new File("D:\\currentproject\\sanri-tools-maven\\requests\\ESApi.http");
        final List<String> lines = FileUtils.readLines(file, StandardCharsets.UTF_8);

        List<RequestInfo> requestInfos = HttpFileParse.parseRequestInfos(lines);

        System.out.println(requestInfos);
    }

    @Test
    public void testJsBinding() throws IOException, ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine scriptEngine = manager.getEngineByExtension("js");
        Bindings bindings = new SimpleBindings();
        scriptEngine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

        CloseableHttpResponse closeableHttpResponse = PoolHttpClient.INSTANCE.sendRequest("https://segmentfault.com/","POST",null, null, null);
        HttpEntity entity = closeableHttpResponse.getEntity();
        String content = EntityUtils.toString(entity);
        RestClientResponse response = RestClientResponseImpl.createResponse(closeableHttpResponse,content );
        HttpClientSession session = HttpClientSession.create(response);
        bindings.put("HTTP_CLIENT_SESSION",session);

        boolean isJson = StringUtils.equalsIgnoreCase(response.getContentType().getMimeType(), ContentType.APPLICATION_JSON.getMimeType());
        String responseBody = isJson ? "JSON.parse(HTTP_CLIENT_SESSION.content)" : "HTTP_CLIENT_SESSION.content";
        String variableDefinition = "var client = { global: HTTP_CLIENT_SESSION.client.global };client.test = function(name, func) { return HTTP_CLIENT_SESSION.client.test(name, func || null);};client.assert = function(res, message) { return HTTP_CLIENT_SESSION.client.assertTrue(res, message || \"Assert failed\");};client.log = function(text) { print(text); };var response = { body: " + responseBody + ", status: HTTP_CLIENT_SESSION.statusCode, headers: HTTP_CLIENT_SESSION.headers, contentType: HTTP_CLIENT_SESSION.contentType};";

        scriptEngine.eval(variableDefinition + "client.log(1); client.global.set('1','2')");

     }
}

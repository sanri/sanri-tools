package com.sanri.tools.bigfileupload.storage;


import com.sanri.tools.bigfileupload.entity.BigFileMeta;

/**
 * 文件元数据管理
 */
public interface MetaManager {

    /**
     * 使用 md5 查询出原始文件
     * @param md5
     * @return
     */
    BigFileMeta selectOriginFile(String md5);

    /**
     * 存储文件元数据
     * @param copyFile
     */
    void saveMeta(BigFileMeta copyFile);
}

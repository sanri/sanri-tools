package com.sanri.tools.bigfileupload.storage;

import com.sanri.tools.bigfileupload.dtos.FileMeta;

import java.io.IOException;

public interface FileStorage {

    /**
     * 追加上传一段
     * @param bucket
     * @param fileId
     * @param part
     */
    void appendPart(String bucket, String fileId, byte [] part) throws IOException;

    /**
     * 获取文件元数据
     * @param bucket
     * @param fileId
     * @return
     */
    FileMeta fileMeta(String bucket, String fileId) throws IOException;
}

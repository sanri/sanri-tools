package com.sanri.tools.bigfileupload.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 文件记录表
 * </p>
 *
 * @author huangzhengri
 * @since 2023-11-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BigFileMeta implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 原始文件名
     */
    private String originName;

    /**
     * 存储桶
     */
    private String bucket;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 文件摘要
     */
    private String md5;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 秒传文件, 链接另一相同文件
     */
    private String link;

    /**
     * 完成进度
     */
    private Long position;
}

package com.sanri.tools.bigfileupload.dtos;

import lombok.Data;

@Data
public class SecondUploadReq {
    private String md5;
    private String originFileName;
    private String bucket;
    private Long fileSize;
}

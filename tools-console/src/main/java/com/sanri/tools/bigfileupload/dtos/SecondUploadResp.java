package com.sanri.tools.bigfileupload.dtos;

import com.sanri.tools.bigfileupload.entity.BigFileMeta;
import lombok.Data;

@Data
public class SecondUploadResp {
    /**
     * 是否成功秒传
     */
    private boolean success;
    /**
     * 秒传文件信息
     */
    private BigFileMeta bigFileMeta;

    public SecondUploadResp() {
    }

    public SecondUploadResp(boolean success) {
        this.success = success;
    }

    public SecondUploadResp(boolean success, BigFileMeta bigFileMeta) {
        this.success = success;
        this.bigFileMeta = bigFileMeta;
    }
}

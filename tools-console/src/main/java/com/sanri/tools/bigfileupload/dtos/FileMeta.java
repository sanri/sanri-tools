package com.sanri.tools.bigfileupload.dtos;

import lombok.Data;

/**
 * 存储软件给的文件元数据
 */
@Data
public class FileMeta {
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 文件 md5
     */
    private String md5;
}

package com.sanri.tools.bigfileupload;

import com.sanri.tools.bigfileupload.dtos.FileMeta;
import com.sanri.tools.bigfileupload.dtos.SecondUploadReq;
import com.sanri.tools.bigfileupload.dtos.SecondUploadResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/bigfile")
public class BigFileUploadController {

    @Autowired
    private BigFileUploadService bigFileUploadService;

    /**
     * 秒传文件
     * @param md5
     * @return
     */
    @PostMapping("secondUpload")
    public SecondUploadResp secondUpload(@RequestBody SecondUploadReq secondUploadReq){
        return bigFileUploadService.secondUpload(secondUploadReq);
    }

    /**
     * 获取文件真实位置
     * @param md5
     */
    @GetMapping("/filePosition")
    public long filePosition(String md5) throws IOException {
        final FileMeta fileMeta = bigFileUploadService.filePosition(md5);
        if (fileMeta == null){
            return 0;
        }
        return fileMeta.getFileSize();
    }

    /**
     * 追加一段
     * @param file 文件
     * @param md5
     */
    @PostMapping("/appendPart")
    public void appendPart(@RequestParam("file") MultipartFile file, String md5) throws IOException {
        bigFileUploadService.appendPart(md5, file.getBytes());
    }

    /**
     * 文件校验
     * @param md5
     * @return
     */
    @PostMapping("/validateFile")
    public void validateFile(String md5) throws IOException {
        bigFileUploadService.validateFile(md5);
    }
}

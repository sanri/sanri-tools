package com.sanri.tools.bigfileupload.storage;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sanri.tools.bigfileupload.entity.BigFileMeta;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class MemMetaManager implements MetaManager{

    List<BigFileMeta> bigFileMetas = new ArrayList<>();

    @Override
    public BigFileMeta selectOriginFile(String md5) {
        final Optional<BigFileMeta> origin = bigFileMetas.stream().filter(item -> item.getMd5().equals(md5) && item.getLink().equals("origin")).findFirst();
        if (!origin.isPresent()){
            return null;
        }
        final BigFileMeta bigFileMeta = origin.get();

        return bigFileMeta;
    }

    @Override
    public void saveMeta(BigFileMeta copyFile) {
        copyFile.setId(IdWorker.getIdStr());
        bigFileMetas.add(copyFile);
    }
}

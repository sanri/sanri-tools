package com.sanri.tools.modules.redis.service.dtos;

import com.sanri.tools.modules.redis.dtos.in.ConnParam;
import com.sanri.tools.modules.redis.dtos.in.SerializerParam;
import lombok.Data;

@Data
public class DataSetDto {
    private ConnParam connParam;
    private String key;
    private String value;
    private String type;
    private SerializerParam serializerParam;
    private String classloaderName;
    private String className;
}

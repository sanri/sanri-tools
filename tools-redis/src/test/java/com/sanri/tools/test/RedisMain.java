package com.sanri.tools.test;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import org.springframework.util.ReflectionUtils;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.util.Slowlog;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

public class RedisMain {

    @Test
    public void testBigKey2(){
        String pattern = "platform_51_access_token_prefix*";
        long size = 100;
        String script = "local large_keys = {}\n" +
                "local cursor = \"0\"\n" +
                "repeat\n" +
                "    local result = redis.call(\"SCAN\", cursor, \"MATCH\", KEYS[1], \"COUNT\", 1000)\n" +
                "    cursor = result[1]\n" +
                "    local keys = result[2]\n" +
                "    for i = 1, #keys do\n" +
                "        local key_len = redis.call(\"memory\",\"usage\", keys[i])\n" +
                "        if key_len > tonumber(ARGV[1]) then\n" +
                "            table.insert(large_keys, keys[i])\n" +
                "            table.insert(large_keys, key_len)\n"+
                "        end\n" +
                "    end\n" +
                "until cursor == \"0\"\n" +
                "return large_keys";
        Jedis jedis = new Jedis("192.168.181.54",6379);
        final Object eval = jedis.eval(script,Arrays.asList(pattern),Arrays.asList(size+""));
        System.out.println(eval);
    }

    @Test
    public void testBigKey(){
        String script = "local large_keys = {} ;" +
                "local cursor = 0 ;" +
                "    local keys = redis.call('keys', '*') ;" +
                "    for i = 1, #keys do" +
                "        local key_len = redis.call('memory','usage', keys[i]) ;" +
                "        if key_len > 100 then " +
//                "            large_keys[i] = keys[i] ;" +
                "              large_keys[keys[i]] = key_len;" +
                "        end;" +
                "    end;" +
                "return large_keys ;";

        Jedis jedis = new Jedis("192.168.181.54",6379);
        final Object eval = jedis.eval(script);
        System.out.println(eval);
    }

    /**
     * 查看某个 key 的内存使用大小
     */
    @Test
    public void testMemoryUsage(){
        Jedis jedis = new Jedis("192.168.181.54",6379);
//        final Set<String> keys = jedis.keys("*");
//        final Client client = jedis.getClient();
//        final Method sendCommand = ReflectionUtils.findMethod(Connection.class, "sendCommand", Protocol.Command.class, byte[][].class);
//        ReflectionUtils.invokeMethod(sendCommand, client, new byte[][]{"memory usage".getBytes(), "430103999999203_0_0_20210528085101748642pack".getBytes()});
//        final Long integerReply = client.getIntegerReply();
//        System.out.println(keys);

        String [] keys = new String[] {"430103999999203_0_0_20210528085101748642pack","202209281130158437280291425_appEnterprise"};

//        final Object eval = jedis.eval("return redis.call('memory','usage',KEYS[1])", 1, "430103999999203_0_0_20210528085101748642pack");
//        System.out.println(eval);

        String script = "local len = #KEYS; local array = {};" +
                "for i=1, len do  " +
                "array[i] = redis.call('memory','usage',KEYS[i])" +
                " end;" +
                "return array" +
                "";

        final Object eval = jedis.eval(script, keys.length, keys);
        System.out.println(eval);
    }



    @Test
    public void slowlog(){
        Jedis jedis = new Jedis("192.168.0.134",6379);
        List<Slowlog> slowlogs = jedis.slowlogGet();
        System.out.println(slowlogs);
        jedis.close();
    }

    public void testVersion(){
        Jedis jedis = new Jedis("192.168.61.71",6699);
        jedis.auth("Redis@1234@pAssword");
    }

    @Test
    public void testScan(){
        Jedis jedis = new Jedis("10.101.40.74",7000);
        jedis.auth("aiVK8ffFCV71L14h");

        Set<String> keys = jedis.keys("sanri*");
        System.out.println(keys);
    }

    @Test
    public void testClusterScan(){
        Set<HostAndPort> hostAndPorts = new HashSet<>();
        for (int i = 0; i < 3; i++) {
//            Jedis jedis = new Jedis("10.101.40.74",7000 + i);
//            jedis.auth("aiVK8ffFCV71L14h");
            hostAndPorts.add(new HostAndPort("10.101.40.74",7000+i));
        }
        for (int i = 0; i < 3; i++) {
            hostAndPorts.add(new HostAndPort("10.101.40.75",7000+i));
        }

        JedisCluster jedisCluster = new JedisCluster(hostAndPorts,2000, 5, 5, "aiVK8ffFCV71L14h", new GenericObjectPoolConfig());
//        ScanParams match = new ScanParams().count(1000).match("sanri*");
//        ScanResult<String> scan = jedisCluster.scan("0", match);
//        List<String> result = scan.getResult();
//        String stringCursor = scan.getStringCursor();
//        System.out.println("cursor:"+stringCursor);
//        System.out.println(result);

        jedisCluster.getClusterNodes().forEach((host,nodes) -> {
            Jedis resource = nodes.getResource();
            Set<String> keys = resource.keys("sanri*");
            System.out.println(keys);
        });
    }

    /**
     * 转移 redis 数据
     */
    @Test
    public void transRedisData() throws IOException {
        Jedis source = new Jedis("localhost",6379);
//        source.auth("liuyang@Redis#!^@199");
//        Jedis target = new Jedis("localhost",6379);
        HostAndPort hostAndPort = new HostAndPort("192.168.31.100",6379);
        HostAndPort hostAndPort1 = new HostAndPort("192.168.31.100",6380);
        HostAndPort hostAndPort2 = new HostAndPort("192.168.31.100",6381);
        HostAndPort hostAndPort3 = new HostAndPort("192.168.31.100",6389);
        HostAndPort hostAndPort4 = new HostAndPort("192.168.31.100",6390);
        HostAndPort hostAndPort5 = new HostAndPort("192.168.31.100",6391);
        final HashSet<HostAndPort> hostAndPorts = new HashSet<>();
        hostAndPorts.add(hostAndPort);
        hostAndPorts.add(hostAndPort1);
        hostAndPorts.add(hostAndPort2);
        hostAndPorts.add(hostAndPort3);
        hostAndPorts.add(hostAndPort4);
        hostAndPorts.add(hostAndPort5);
        JedisCluster target = new JedisCluster(hostAndPorts);

        int j = 0;
        for (int i = 0; i < 16; i++) {
            source.select(i);
//            target.select(i);
            System.out.println("移动数据库:"+i+" 数据量:"+source.dbSize());

            final Set<String> keys = source.keys("*");
            for (String key : keys) {
                final String type = source.type(key);
                System.out.println("移动 key:"+key+"类型:"+type+" 总共移动:"+(++j));

                if ("string".equals(type)){
                    target.set(key,source.get(key));
                }else if ("hash".equals(type)){
                    final Set<String> hkeys = source.hkeys(key);
                    for (String hkey : hkeys) {
                        target.hset(key,hkey,source.hget(key,hkey));
                    }
                }else if ("list".equals(type)){
                    final List<String> lrange = source.lrange(key, 0, source.llen(key));
                    for (String value : lrange) {
                        target.lpush(key,value);
                    }
                }else if ("set".equals(type)){
                    final Set<String> smembers = source.smembers(key);
                    for (String smember : smembers) {
                        target.sadd(key,smember);
                    }
                }else if ("zset".equals(type)){
                    final Set<String> zrange = source.zrange(key, 0, source.zcard(key));
                    for (String value : zrange) {
                        target.zadd(key,source.zscore(key,value),value);
                    }
                }
            }

            source.close();
        }

        target.close();
    }

    @Test
    public void setValue(){
        String key = "GA1400:DEVICE:ONLINE:43010000001320000210";
        Jedis jedis = new Jedis("192.168.16.148");
        jedis.auth("123456");
        jedis.connect();
        jedis.set(key , String.valueOf(new Date().getTime()));
    }
}
